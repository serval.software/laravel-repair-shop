<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/consulta', 'ConsultaController@consulta');
Route::get('/orden', 'ConsultaController@orden');

Auth::routes([ 'register' => false ]);

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/ordens/filtro', 'OrdenController@filter')->middleware('auth');
Route::get('/ordens/buscar', 'OrdenController@search')->middleware('auth');
Route::get('/ordens/imprimir/{id}', 'OrdenController@imprimir')->middleware('auth');

Route::get('/precios/filtro', 'PrecioController@filter')->middleware('auth');

Route::get('/clientes/search', 'ClienteController@search')->middleware('auth');
Route::get('/clientes/buscar', 'ClienteController@buscar')->middleware('auth');
Route::get('/clientes/filtro', 'ClienteController@filter')->middleware('auth');

Route::resource('ordens','OrdenController')->middleware('auth');
Route::resource('clientes','ClienteController')->middleware('auth');
Route::resource('precios','PrecioController')->middleware('auth');
Route::resource('externos','ExternoController')->middleware('auth');
Route::resource('users','UserController')->middleware('auth');