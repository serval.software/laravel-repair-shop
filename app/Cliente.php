<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'dni', 'celular', 'alternativo', 'email', 'domicilio'];

    public static function boot() {
        parent::boot();

        static::deleting(function($cliente) {
            // Borra las órdenes asociadas:
            // $cliente->ordens()->delete();
        });
    }

    public function ordens()
    {
        $ordens = $this->hasMany('App\Orden')->orderBy('updated_at', 'desc');
        //dd($ordens);
        return $ordens;
    }
}
