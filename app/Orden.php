<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orden extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at', 'retirado_at', 'fecha_presupuesto', 'fecha_revision'];
    protected $fillable = [ 
        'cliente_id', 
        'mostrador_user_id',
        'producto',
        'marca', 
        'modelo', 
        'serie', 
        'problema', 
        'observaciones',
        'presupuesto', 
        'ubicacion', 
        'estado',
        'seguimiento',
        'tecnico_user_id',
        'revision',
        'fecha_revision',
        'en_garantia',
        'admin_user_id',
        'presupuesto',
        'fecha_presupuesto',
        'permitir_consultas',
        'retirado_at'
    ];

    public static function generarCodigo($orden_id) {
        try {
            $orden = Orden::find($orden_id);
            $orden->codigo = hash('crc32b', uniqid());
            $orden->save();
    
        } catch (Exception $e) {
            $error_info = $e->errorInfo;
            if($error_info[1] == 1062) {
                generarCodigo($orden_id);
            } else {
                // Only logs when an error other than duplicate happens
                Log::error($e);
            }
    
        }
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente')->withTrashed();
    }

    public function mostrador_user()
    {
        return $this->belongsTo('App\User', 'mostrador_user_id')->withTrashed();
    }

    public function tecnico_user()
    {
        return $this->belongsTo('App\User', 'tecnico_user_id')->withTrashed();
    }

    public function admin_user()
    {
        return $this->belongsTo('App\User', 'admin_user_id')->withTrashed();
    }

    const INGRESADO_A_REVISAR         = 1;
    const REVISADO_A_PRESUPUESTAR     = 2;
    const PRESUPUESTADO_A_PREGUNTAR   = 3;
    const EN_ESPERA_DE_RESPUESTA      = 4;
    const A_REPARAR                   = 5;
    const REPRESUPUESTAR              = 6;
    const EN_REPARACION               = 7;
    const EN_PREPARACION              = 8;
    const PARA_RETIRAR_AVISAR         = 9;
    const PARA_RETIRAR_AVISADO        = 10;
    const PLAZO_VENCIDO_AVISADO       = 11;
    const RETIRADO_REPARADO           = 12;
    const RETIRADO_NO_REPARADO        = 13;
    const NO_RETIRADO_RECICLADO       = 14;
    const A_RECICLAR                  = 15;

    /**
     * Return list of status codes and labels

     * @return array
     */
    public static function listarEstados()
    {
        return [
            self::INGRESADO_A_REVISAR => 'Ingresado, a revisar',
            self::REVISADO_A_PRESUPUESTAR => 'Revisado, a presupuestar',
            self::PRESUPUESTADO_A_PREGUNTAR => 'Presupuestado, a preguntar',
            self::EN_ESPERA_DE_RESPUESTA => 'En espera de respuesta',
            self::A_RECICLAR => 'Para reciclar',
            self::A_REPARAR => 'A reparar',
            self::REPRESUPUESTAR => 'Represupuestar',
            self::EN_REPARACION => 'En reparación',
            self::EN_PREPARACION => 'En preparación',
            self::PARA_RETIRAR_AVISAR => 'Para retirar, avisar',
            self::PARA_RETIRAR_AVISADO => 'Para retirar, avisado',
            self::PLAZO_VENCIDO_AVISADO => 'Plazo vencido, avisado',
            self::RETIRADO_REPARADO => 'Retirado (reparado)',
            self::RETIRADO_NO_REPARADO => 'Retirado (sin reparar)',
            self::NO_RETIRADO_RECICLADO => 'No retirado (reciclado)'
        ];
    }

    /**
     * Devuelve el nombre para mostrar del estado

     * @param string
     */
    public function estadoNombre()
    {
        $list = self::listarEstados();

        // validación por si hay un estado inexistente guardado en la db
        return isset($list[$this->estado]) 
            ? $list[$this->estado] 
            : $this->estado;
    }

    /**
     * Some actions will happen only if it's active, so I have 
     * this method for making things easier.
     * Other status doesn't have a specific method because
     * I usually don't compare agains them
     * @return Boolean
     */
    public function isActive()
    {
        return $this->estado == self::EN_REPARACION;
    }

    /**
     * Solo ordenes con ciertos estados
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  mixed $estado
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeConEstado($query, $estado)
    {
        return $query->where('estado', $estado);
    }
}
