<?php

namespace App\Http\Controllers;

use App\Externo;
use Illuminate\Http\Request;

class ExternoController extends Controller
{
    public function __construct()
    {
        $this->middleware('rol:todos', ['only' => ['index']]);
        $this->middleware('rol:admin', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clases = Externo::listarClases();
        $externos = Externo::all();
        return view('externos.index', compact('externos', 'clases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $clases = Externo::listarClases();
        return view('externos.create', compact('clases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre' => 'required',
            'email' => 'nullable', 
            'telefono' => 'nullable', 
            'direccion' => 'nullable',
            'clase' => 'required'
        ]);

        $externo = Externo::create($request->all());

        return redirect()->action('ExternoController@index')
            ->with('success', 'Externo registrado correctamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Externo $externo)
    {
        //
        $clases = Externo::listarClases();

        return view('externos.edit',compact('externo', 'clases'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Externo $externo)
    {
        //
        $request->validate([
            'nombre' => 'required',
            'email' => 'nullable', 
            'telefono' => 'nullable', 
            'direccion' => 'nullable',
            'clase' => 'required'
        ]);
        $externo->update($request->all());
        return redirect()->action('ExternoController@index')
                         ->with('success','Los datos del externo fueron modificados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Externo $externo)
    {
        //
        $externo->delete();
    }
}
