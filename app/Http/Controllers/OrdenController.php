<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orden;
use App\Repositories\UserRepository;

class OrdenController extends Controller
{
    public function __construct()
    {
        $this->middleware('rol:todos', ['only' => ['index', 'show', 'filter', 'edit', 'update', 'search', 'imprimir']]);
        $this->middleware('rol:admin|mostrador', ['only' => ['create', 'store']]);
        $this->middleware('rol:admin', ['only' => ['destroy']]);
    }

    public function index()
    {
        //
        $ordens = Orden::with('cliente:id,nombre')->orderBy('id', 'desc')->paginate(20);
        $estados = Orden::listarEstados();
        $filtrado = false;

        return view('ordens.index', compact('ordens', 'estados', 'filtrado'));
    }

    public function imprimir($id)
    {
        $orden = Orden::with('cliente')->find($id);

        return view('ordens.imprimir', compact('orden'));
    }

    public function search()
    {
        $estados = Orden::listarEstados();
        $users = UserRepository::getActiveUsers();

        return view('ordens.buscar', compact('estados', 'users'));
    }

    public function filter(Request $request)
    {
        //
        $grupo = $request->input('grupo');

        $id = $request->input('id');
        $codigo = $request->input('codigo');
        $estado = $request->input('estado');
        $en_garantia = $request->input('en_garantia');
        $mostrador_user_id = $request->input('mostrador_user_id');
        $tecnico_user_id = $request->input('tecnico_user_id');
        $admin_user_id = $request->input('admin_user_id');

        $producto = $request->input('producto');
        $marca = $request->input('marca');
        $modelo = $request->input('modelo');
        $serie = $request->input('serie');
        $problema = $request->input('problema');
        $observaciones = $request->input('observaciones');
        $ubicacion = $request->input('ubicacion');
        $seguimiento = $request->input('seguimiento');
        $revision = $request->input('revision');
        $presupuesto = $request->input('presupuesto');

        $created_at_inicio = $request->input('created_at-inicio');
        $created_at_fin = $request->input('created_at-fin');
        $retirado_at_inicio = $request->input('retirado_at-inicio');
        $retirado_at_fin = $request->input('retirado_at-fin');
        $fecha_revision_inicio = $request->input('fecha_revision-inicio');
        $fecha_revision_fin = $request->input('fecha_revision-fin');
        $fecha_presupuesto_inicio = $request->input('fecha_presupuesto-inicio');
        $fecha_presupuesto_fin = $request->input('fecha_presupuesto-fin');
        
        $ordens = Orden::with('cliente:id,nombre')
                ->when($grupo, function ($query, $grupo) {
                    if ($grupo === "revision") {
                        return $query
                            ->where('estado', Orden::INGRESADO_A_REVISAR);
                    } elseif ($grupo === "presupuestos") {
                        return $query
                            ->where('estado', Orden::REVISADO_A_PRESUPUESTAR)
                            ->orWhere('estado', Orden::REPRESUPUESTAR);
                    } elseif ($grupo === "comunicacion") {
                        return $query
                            ->where('estado', Orden::PRESUPUESTADO_A_PREGUNTAR)
                            ->orWhere('estado', Orden::PARA_RETIRAR_AVISAR)
                            ->orWhere('estado', Orden::EN_ESPERA_DE_RESPUESTA);
                    } elseif ($grupo === "reparacion") {
                        return $query
                            ->where('estado', Orden::A_REPARAR)
                            ->orWhere('estado', Orden::EN_REPARACION)
                            ->orWhere('estado', Orden::EN_PREPARACION);
                    } elseif ($grupo === "retirar") {
                        return $query
                            ->where('estado', Orden::PARA_RETIRAR_AVISADO)
                            ->orWhere('estado', Orden::PLAZO_VENCIDO_AVISADO);
                    } elseif ($grupo === "retirados") {
                        return $query
                            ->where('estado', Orden::RETIRADO_REPARADO)
                            ->orWhere('estado', Orden::RETIRADO_NO_REPARADO);
                    } elseif ($grupo === "sinretirar") {
                        return $query
                            ->where('estado', Orden::NO_RETIRADO_RECICLADO)
                            ->orWhere('estado', Orden::A_RECICLAR);
                    }
                    
                })
                ->when($id, function ($query, $id) {
                    return $query->where('id', $id);
                })
                ->when($codigo, function ($query, $codigo) {
                    return $query->where('codigo', $codigo);
                })
                ->when($en_garantia, function ($query, $en_garantia) {
                    return $query->where('en_garantia', $en_garantia);
                })
                ->when($mostrador_user_id, function ($query, $mostrador_user_id) {
                    return $query->where('mostrador_user_id', $mostrador_user_id);
                })
                ->when($tecnico_user_id, function ($query, $tecnico_user_id) {
                    return $query->where('tecnico_user_id', $tecnico_user_id);
                })
                ->when($admin_user_id, function ($query, $admin_user_id) {
                    return $query->where('admin_user_id', $admin_user_id);
                })
                ->when($producto, function ($query, $producto) {
                    return $query->where('producto', 'LIKE', '%' . $producto . '%');
                })
                ->when($marca, function ($query, $marca) {
                    return $query->where('marca', 'LIKE', '%' . $marca . '%');
                })
                ->when($modelo, function ($query, $modelo) {
                    return $query->where('modelo', 'LIKE', '%' . $modelo . '%');
                })
                ->when($serie, function ($query, $serie) {
                    return $query->where('serie', 'LIKE', '%' . $serie . '%');
                })
                ->when($problema, function ($query, $problema) {
                    return $query->where('problema', 'LIKE', '%' . $problema . '%');
                })
                ->when($observaciones, function ($query, $observaciones) {
                    return $query->where('observaciones', 'LIKE', '%' . $observaciones . '%');
                })
                ->when($ubicacion, function ($query, $ubicacion) {
                    return $query->where('ubicacion', 'LIKE', '%' . $ubicacion . '%');
                })
                ->when($seguimiento, function ($query, $seguimiento) {
                    return $query->where('seguimiento', 'LIKE', '%' . $seguimiento . '%');
                })
                ->when($revision, function ($query, $revision) {
                    return $query->where('revision', 'LIKE', '%' . $revision . '%');
                })
                ->when($presupuesto, function ($query, $presupuesto) {
                    return $query->where('presupuesto', 'LIKE', '%' . $presupuesto . '%');
                })
                ->when($estado, function ($query, $estado) {
                    return $query->where('estado', $estado);
                })
                ->when($created_at_inicio, function ($query, $created_at_inicio) {
                    return $query->whereDate('created_at', '>=', $created_at_inicio);
                })
                ->when($created_at_fin, function ($query, $created_at_fin) {
                    return $query->whereDate('created_at', '<=', $created_at_fin);
                })
                ->when($retirado_at_inicio, function ($query, $retirado_at_inicio) {
                    return $query->whereDate('retirado_at', '>=', $retirado_at_inicio);
                })
                ->when($retirado_at_fin, function ($query, $retirado_at_fin) {
                    return $query->whereDate('retirado_at', '<=', $retirado_at_fin);
                })
                ->when($fecha_presupuesto_inicio, function ($query, $fecha_presupuesto_inicio) {
                    return $query->whereDate('fecha_presupuesto', '>=', $fecha_presupuesto_inicio);
                })
                ->when($fecha_presupuesto_fin, function ($query, $fecha_presupuesto_fin) {
                    return $query->whereDate('fecha_presupuesto', '<=', $fecha_presupuesto_fin);
                })
                ->when($fecha_revision_inicio, function ($query, $fecha_revision_inicio) {
                    return $query->whereDate('fecha_revision', '>=', $fecha_revision_inicio);
                })
                ->when($fecha_revision_fin, function ($query, $fecha_revision_fin) {
                    return $query->whereDate('fecha_revision', '<=', $fecha_revision_fin);
                })
                ->orderBy('id', 'desc')->paginate(20);

        $estados = Orden::listarEstados();

        $filtrado = true;

        return view('ordens.index', compact('ordens', 'estados', 'filtrado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ordens.create');
    }

    
    public function store(Request $request)
    {
        //
        $request->validate([
            'producto' => 'required',
            'problema' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'serie' => 'required',
            'ubicacion' => 'nullable',
            'observaciones' => 'nullable'
        ]);

        $orden = $request->all();

        $orden['mostrador_user_id'] = auth()->user()->id;

        $orden['estado'] = 1;

        $id = Orden::create($orden)->id;

        Orden::generarCodigo($id);

        return back()->with('success', 'Orden registrada correctamente');;
    }
    
    public function show(Orden $orden)
    {
        //
        return view('ordens.show', compact('orden'));
    }
    
    public function edit(Orden $orden)
    {
        //
        $users = UserRepository::getActiveUsers();
        $estados = Orden::listarEstados();
        if (auth()->user()->rol == 'admin') {
            $permiso = [
                'general' => true,
                'mostrador' => true,
                'tecnico' => true,
                'admin' => true
            ];
        } elseif (auth()->user()->rol == 'mostrador') {
            $permiso = [
                'general' => true,
                'mostrador' => true,
                'tecnico' => false,
                'admin' => false
            ];
        } elseif (auth()->user()->rol == 'tecnico') {
            $permiso = [
                'general' => true,
                'mostrador' => false,
                'tecnico' => true,
                'admin' => false
            ];
        } else {
            $permiso = [
                'general' => false,
                'mostrador' => false,
                'tecnico' => false,
                'admin' => false
            ];
        }


        return view('ordens.edit', compact('orden', 'estados', 'users', 'permiso'));
    }
    
    public function update(Request $request, Orden $orden)
    {
        //
        if (auth()->user()->rol === "admin") {
            $request->validate([
                'producto' => 'required',
                'marca' => 'required',
                'modelo' => 'required',
                'serie' => 'required',
                'problema' => 'required',
                'observaciones' => 'nullable',
                'estado' => 'required',
                'mostrador_user_id' => 'required',
                'ubicacion' => 'nullable',
                'seguimiento' => 'nullable',
                'tecnico_user_id' => 'nullable',
                'revision' => 'nullable',
                'fecha_revision' => 'nullable',
                'en_garantia' => 'nullable',
                'admin_user_id' => 'nullable',
                'presupuesto' => 'nullable',
                'fecha_presupuesto' => 'nullable',
                'permitir_consultas' => 'nullable',
                'retirado_at' => 'nullable'
            ]);
        }elseif (auth()->user()->rol === "tecnico") {
            $request->validate([
                'estado' => 'required',
                'ubicacion' => 'nullable',
                'seguimiento' => 'nullable',
                'tecnico_user_id' => 'nullable',
                'revision' => 'nullable',
                'fecha_revision' => 'nullable',
                'en_garantia' => 'nullable'
            ]);
        }elseif (auth()->user()->rol === "mostrador") {
            $request->validate([
                'producto' => 'required',
                'marca' => 'required',
                'modelo' => 'required',
                'serie' => 'required',
                'problema' => 'required',
                'observaciones' => 'nullable',
                'estado' => 'required',
                'mostrador_user_id' => 'required',
                'ubicacion' => 'nullable',
                'seguimiento' => 'nullable',
                'retirado_at' => 'nullable'
            ]);
        }
        
        $orden->update($request->all());
        return redirect()->action('OrdenController@show', compact('orden'))
            ->with('success', 'Los datos de la orden fueron modificados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orden $orden)
    {
        $orden->delete();
        return redirect('ordens')->with('success','La orden fue borrada correctamente');
    }
}
