<?php

namespace App\Http\Controllers;

use App\Precio;
use App\Externo;
use Illuminate\Http\Request;

class PrecioController extends Controller
{
    public function __construct()
    {
        $this->middleware('rol:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $precios = Precio::orderBy('categoria', 'asc')->paginate(20);
        $categorias = Precio::listarCategorias();
        $proveedores = Externo::getProveedoresExternos();

        $filtrado = false;

        return view('precios.index', compact('precios', 'categorias', 'proveedores', 'filtrado'));
    }

    public function filter(Request $request)
    {
        //
        $id = $request->input('id');
        $marca = $request->input('marca');
        $modelo = $request->input('modelo');
        $parte = $request->input('parte');
        $categoria = $request->input('categoria');
        $externo_id = $request->input('externo_id');
        $fecha_inicio = $request->input('fecha-inicio');
        $fecha_fin = $request->input('fecha-fin');
        
        $precios = Precio::with('proveedor:id,nombre')
                ->where('marca', 'LIKE', '%' . $request->input('marca') . '%')
                ->where('modelo', 'LIKE', '%' . $request->input('modelo') . '%')
                ->where('parte', 'LIKE', '%' . $request->input('parte') . '%')
                ->when($categoria, function ($query, $categoria) {
                    return $query->where('categoria', $categoria);
                })
                ->when($externo_id, function ($query, $externo_id) {
                    return $query->where('externo_id', $externo_id);
                })
                ->when($fecha_inicio, function ($query, $fecha_inicio) {
                    return $query->whereDate('fecha_precio', '>=', $fecha_inicio);
                })
                ->when($fecha_fin, function ($query, $fecha_fin) {
                    return $query->whereDate('fecha_precio', '<=', $fecha_fin);
                })
                ->orderBy('categoria', 'asc')->paginate(20);

        $categorias = Precio::listarCategorias();
        $proveedores = Externo::getProveedoresExternos();

        $filtrado = true;

        return view('precios.index', compact('precios', 'categorias', 'proveedores', 'filtrado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorias = Precio::listarCategorias();
        $externos = Externo::getProveedoresExternos();
        return view('precios.create', compact('categorias', 'externos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'categoria' => 'required',
            'marca' => 'nullable',
            'modelo' => 'nullable',
            'parte' => 'required',
            'precio' => 'required',
            'precio_cliente' => 'nullable',
            'externo_id' => 'nullable',
            'fecha_precio' => 'nullable'
        ]);

        $precio = Precio::create($request->all());
        
        return redirect()->action('PrecioController@index')
            ->with('success', 'Precio registrado correctamente');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Precio $precio)
    {
        //
        $categorias = Precio::listarCategorias();
        $externos = Externo::getProveedoresExternos();
        return view('precios.edit', compact('precio', 'categorias', 'externos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Precio $precio)
    {
        //
        $request->validate([
            'categoria' => 'required',
            'marca' => 'nullable',
            'modelo' => 'nullable',
            'parte' => 'required',
            'precio' => 'required',
            'precio_cliente' => 'nullable',
            'externo_id' => 'nullable',
            'fecha_precio' => 'nullable'
        ]);
        
        $precio->update($request->all());
        return redirect()->action('PrecioController@index')
            ->with('success', 'Los datos del precio fueron modificados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Precio $precio)
    {
        //
        $precio->delete();
        return redirect('precios')->with('success','El precio fue borrado correctamente');
        
    }
}
