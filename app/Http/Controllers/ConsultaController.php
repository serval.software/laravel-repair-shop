<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orden;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ConsultaController extends Controller
{
    //
    public function consulta () {
        $busquedaFail = false;
        return view('consulta', compact('busquedaFail'));
    }

    public function orden (Request $request) {
        $codigo = $request->input('codigo');

        try
        {
            $orden = Orden::with('cliente')->where('permitir_consultas', 1)->where('codigo', $codigo)->firstOrFail();
        }
        catch(ModelNotFoundException $e)
        {
            $busquedaFail = true;
            return view('consulta', compact('busquedaFail'));
        }
        
        $estados = Orden::listarEstados();
        return view('orden', compact('orden', 'estados'));
    } 
}
