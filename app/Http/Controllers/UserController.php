<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('rol:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'rol' => ['required', 'string'],
        ]);
        $hashed =  Hash::make($request->input('password'));
        $request->merge(['password' => $hashed]);

        $user = User::create($request->all());
        
        return redirect()->action('UserController@index')
            ->with('success', 'Usuario registrado correctamente');;
    }

    public function show(User $user)
    {
        //
        return view('users.show', compact('user'));
    }
    
    public function edit(User $user)
    {
        //
        return view('users.edit',compact('user'));
    }
    
    public function update(Request $request, User $user)
    {
        //
        $request->validate([
            'name' => 'required',
            'email' => 'required', 
            'rol' => 'required'
        ]);

        if (auth()->user()->id === $user->id && auth()->user()->rol !== $request->rol) {
            return abort(403, 'Un usuario no puede cambiar su propio rol.');
        }
        $user->update($request->all());
        return redirect('users')->with('success','El usuario fue modificado correctamente');
    }
    
    public function destroy(User $user)
    {
        //
        if (auth()->user()->id !== $user->id) {
            $user->delete();
            return redirect('users')->with('success','El usuario fue borrado correctamente');
        }else{
            return abort(403, 'Un usuario no puede borrarse a sí mismo.');
        }
    }
}
