<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Orden;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('rol:todos', ['only' => ['index', 'show', 'search', 'buscar', 'filter']]);
        $this->middleware('rol:admin|mostrador', ['only' => ['create', 'edit', 'store', 'update']]);
        $this->middleware('rol:admin', ['only' => ['destroy']]);
    }

    public function buscar()
    {
        //
        return view('clientes.buscar');
    }

    public function filter(Request $request)
    {
        $nombre = $request->input('nombre');
        $dni = $request->input('dni');
        $email = $request->input('email');
        $celular = $request->input('celular');
        $alternativo = $request->input('alternativo');
        $domicilio = $request->input('domicilio');
        
        $clientes = Cliente::
                when($nombre, function ($query, $nombre) {
                    return $query->where('nombre', 'LIKE', '%' . $nombre . '%');
                })
                ->when($dni, function ($query, $dni) {
                    return $query->where('dni', 'LIKE', '%' . $dni . '%');
                })
                ->when($celular, function ($query, $celular) {
                    return $query->where('celular', 'LIKE', '%' . $celular . '%');
                })
                ->when($alternativo, function ($query, $alternativo) {
                    return $query->where('alternativo', 'LIKE', '%' . $alternativo . '%');
                })
                ->when($email, function ($query, $email) {
                    return $query->where('email', 'LIKE', '%' . $email . '%');
                })
                ->when($domicilio, function ($query, $domicilio) {
                    return $query->where('domicilio', 'LIKE', '%' . $domicilio . '%');
                })
                ->orderBy('id', 'desc')->paginate(20);

        $filtrado = true;
        return view('clientes.index', compact('clientes', 'filtrado'));
    }

    public function search(Request $request)
    {
        $clientes = [];

        // Saca espacios al principio y al final, saca comas, convierte espacios múltiples en simples
        $string = preg_replace( '/\s+/', ' ', preg_replace('/[,]/', '', trim($request->input('buscar-cliente'))));
        
        if(is_numeric($string)) {
            $clientes = Cliente::where('dni', $string)
               ->orderBy('nombre', 'asc')
               ->take(100)
               ->orderBy('id', 'desc')->paginate(20);
            if($clientes->count() === 0) {
                $clientes = Cliente::where('celular', 'like', '%' . $string . '%')
               ->orderBy('nombre', 'asc')
               ->take(100)
               ->orderBy('id', 'desc')->paginate(20);
            }
            if($clientes->count() === 0) {
                $clientes = Cliente::where('alternativo', 'like', '%' . $string . '%')
               ->orderBy('nombre', 'asc')
               ->take(100)
               ->orderBy('id', 'desc')->paginate(20);
            }
        }else{
            $terminos = explode(' ', $string);

            foreach ($terminos as $index => $termino) {
                $queryTermino[$index] = '%'.$termino.'%';
                $queryNombre[] = ['nombre', 'like', $queryTermino[$index]];
            }
            $clientes = Cliente::where($queryNombre)->orderBy('nombre', 'asc')->take(100)->orderBy('id', 'desc')->paginate(20);

            if($clientes->count() === 0) {
                foreach ($terminos as $index => $termino) {
                    $queryTermino[$index] = '%'.$termino.'%';
                    $queryEmail[] = ['email', 'like', $queryTermino[$index]];
                }
                $clientes = Cliente::where($queryEmail)->orderBy('nombre', 'asc')->take(100)->orderBy('id', 'desc')->paginate(20);
            }

            if($clientes->count() === 0) {
                foreach ($terminos as $index => $termino) {
                    $queryTermino[$index] = '%'.$termino.'%';
                    $queryDomicilio[] = ['domicilio', 'like', $queryTermino[$index]];
                }
                $clientes = Cliente::where($queryDomicilio)->orderBy('nombre', 'asc')->take(100)->orderBy('id', 'desc')->paginate(20);
            }
        }
        
        if(count($clientes) === 1){
            return redirect()->action('ClienteController@show', ['id' => $clientes[0]['id']]);
        }else{
            $filtrado = true;
            return view('clientes.index', compact('clientes', 'filtrado'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clientes = Cliente::orderBy('id', 'desc')->paginate(20);
        $filtrado = false;
        return view('clientes.index', compact('clientes', 'filtrado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nombre' => 'required',
            'dni' => 'required',
            'celular' => 'required',
            'alternativo' => 'nullable', 
            'email' => 'nullable', 
            'domicilio' => 'nullable'
        ]);

        $cliente = Cliente::create($request->all());
        
        return redirect()->action('ClienteController@show', ['id' => $cliente->id])
            ->with('success', 'Cliente registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
        $estados = Orden::listarEstados();
        return view('clientes.show', compact('cliente', 'estados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
        return view('clientes.edit',compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        //
        $request->validate([
            'nombre' => 'required',
            'dni' => 'required',
            'celular' => 'required',
            'alternativo' => 'nullable', 
            'email' => 'nullable', 
            'domicilio' => 'nullable'
        ]);
        $cliente->update($request->all());
        return redirect()->action('ClienteController@show', ['id' => $cliente->id])
                         ->with('success','Los datos del cliente fueron modificados correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        //
        $cliente->delete();
        return redirect('clientes')->with('success','El cliente fue borrado correctamente');
        
    }
}
