<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Precio extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at', 'fecha_precio'];
    protected $fillable = [
        'categoria',
        'marca',
        'modelo',
        'parte',
        'precio',
        'precio_cliente',
        'fecha_precio',
        'externo_id'
    ];

    public function proveedor()
    {
        return $this->belongsTo('App\Externo', 'externo_id')->withTrashed();
    }

    const PLACAS    = 1;
    const PANTALLAS = 2;
    const OTROS     = 3;
    
    /**
     * Return list of status codes and labels

     * @return array
     */
    public static function listarCategorias()
    {
        return [
            self::PLACAS => 'Placas',
            self::PANTALLAS => 'Pantallas',
            self::OTROS => 'Otros'
        ];
    }

    /**
     * Devuelve el nombre para mostrar del categoria

     * @param string
     */
    public function categoriaNombre()
    {
        $list = self::listarCategorias();

        // validación por si hay un categoria inexistente guardado en la db
        return isset($list[$this->categoria]) 
            ? $list[$this->categoria] 
            : $this->categoria;
    }
}
