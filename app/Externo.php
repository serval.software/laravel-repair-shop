<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Externo extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'email', 'telefono', 'direccion', 'clase'];

    public static function boot() {
        parent::boot();

        static::deleting(function($externo) {
            // Borra los precios asociados:
            // $externo->precios()->delete();
        });
    }

    public function precios()
    {
        $precios = $this->hasMany('App\Precio');
        return $precios;
    }

    public static function getActiveExternos(){
        return self::all();
    }

    public static function getProveedoresExternos(){
        return self::where('clase', self::PROVEEDOR)->get();
    }

    public static function getTecnicosExternos(){
        return self::where('clase', self::SERVICIO_TECNICO)->get();
    }

    const PROVEEDOR             = 1;
    const SERVICIO_TECNICO      = 2;

    /**
     * Return list of status codes and labels

     * @return array
     */
    public static function listarClases()
    {
        return [
            self::PROVEEDOR => 'Proveedor',
            self::SERVICIO_TECNICO => 'Servicio técnico'
        ];
    }

    /**
     * Devuelve el nombre para mostrar del estado

     * @param string
     */
    public function claseNombre()
    {
        $list = self::listarClases();

        return isset($list[$this->clase]) 
            ? $list[$this->clase] 
            : $this->clase;
    }
}
