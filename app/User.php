<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rol'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ordens()
    {
        $ordens = $this->hasMany('App\Orden')->orderBy('updated_at', 'desc');
        return $ordens;
    }
    
    public function hasRol($rol)
    {
        if ($rol === 'todos') {
            return true;
        }
        $roles = explode('|', $rol);
        foreach($roles as $r){
            if ($this->rol === $r) {
                return true;
            }
        }
        return false;
    }


}
