<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener 6 caracteres y concordar.',
    'reset' => 'Tu contraseña fue restablecida.',
    'sent' => 'Te enviamos por correo electrónico el link de restablecimiento de contraseña.',
    'token' => 'El token de restablecimiento es incorrecto.',
    'user' => "No podemos encontrar un usuario con ese correo electrónico.",

];
