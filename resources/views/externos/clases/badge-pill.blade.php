@if ($externo->clase == App\Externo::PROVEEDOR)
<span class="badge badge-pill badge-success">{{$externo->claseNombre()}}</span>
@elseif ($externo->clase == App\Externo::SERVICIO_TECNICO)
<span class="badge badge-pill badge-warning">{{$externo->claseNombre()}}</span>
@endif