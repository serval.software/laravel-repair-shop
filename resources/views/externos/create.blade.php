@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Registrar nuevo externo</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
    </div><br />
    @endif
    <form method="post" action="{{url('externos')}}">
        {{csrf_field()}}
        <div class="row">
            <div class="form-group col-lg-6">
                <label for="clase">Clase:</label>
                <select class="form-control" name="clase">
                    @foreach ($clases as $index => $clase)
                    <option value="{{$index}}">{{$clase}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6 required">
                <label for="nombre">Nombre:</label>
                <input required type="text" class="form-control" name="nombre">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4">
                <label for="email">Correo electrónico:</label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group col-lg-4">
                <label for="direccion">Dirección:</label>
                <input type="text" class="form-control" name="direccion">
            </div>
            <div class="form-group col-lg-4">
                <label for="telefono">Teléfono:</label>
                <input type="text" class="form-control" name="telefono">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <button type="submit" class="btn btn-success">Registrar externo</button>
            </div>
        </div>
    </form>
</div>
@endsection