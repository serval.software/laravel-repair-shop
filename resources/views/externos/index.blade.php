@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="btn-group">
                <a class="btn btn-primary" href="{{ action('ExternoController@create') }}">Registrar externo</a>
            </div>
        </div>
    </div>
    @if (\Session::has('success'))
        <div class="alert alert-success d-block">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Clase</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($externos as $externo)
                <tr>
                    <td>@include('externos.clases.badge-pill', ['clase' => $externo['clase']])</td>
                    <td><a href="{{ route('externos.edit',$externo['id']) }}">{{$externo['nombre']}}</a></td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Acciones">
                            <a href="{{action('ExternoController@edit', $externo['id'])}}" class="btn btn-warning btn-sm">Editar</a>
                            <form onsubmit="return confirm('¿Realmente desea borrar al proveedor de servicios externo? No borrará los precios asociados.');" action="{{action('ExternoController@destroy', $externo['id'])}}" method="post" class="d-inline-block confirm-needed">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
