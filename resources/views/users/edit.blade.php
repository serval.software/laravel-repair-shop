@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Editar usuario</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{action('UserController@update', $user->id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">

        <div class="row">
            <div class="form-group col-lg-6 mx-auto">
                <label for="name">Nombre:</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-6 mx-auto">
                <label for="email">Correo electrónico:</label>
                <input type="email" class="form-control" name="email" value="{{$user->email}}">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-6 mx-auto">
                <label for="rol">Rol:</label>
                <select id="rol" class="form-control" name="rol">
                    <option value="admin" {{($user->rol=="admin"?"selected":"")}}>Administración</option>
                    <option value="mostrador" {{($user->rol=="mostrador"?"selected":"")}}>Mostrador</option>
                    <option value="tecnico" {{($user->rol=="tecnico"?"selected":"")}}>Técnico</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <button type="submit" class="btn btn-success">Modificar user</button>
            </div>
        </div>
    </form>
</div>
@endsection