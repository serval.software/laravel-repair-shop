@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="btn-group">
                <a class="btn btn-primary" href="{{ action('UserController@create') }}">Crear usuario</a>
            </div>
        </div>
    </div>
    @if (\Session::has('success'))
        <div class="alert alert-success d-block">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Rol</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{ route('users.show',$user['id']) }}">{{$user['name']}}</a></td>
                    <td>{{$user['email']}}</td>
                    <td>{{$user['rol']}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Acciones">
                            <a href="{{action('UserController@show', $user['id'])}}" class="btn btn-info btn-sm">Ver</a>
                            <a href="{{action('UserController@edit', $user['id'])}}" class="btn btn-warning btn-sm">Editar</a>
                            <form onsubmit="return confirm('¿Realmente desea borrar al usuario?');" action="{{action('UserController@destroy', $user['id'])}}" method="post" class="d-inline-block confirm-needed">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
