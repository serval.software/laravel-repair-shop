@extends('layouts.app')

@section('content')
<div class="container justify-content-center">
    <div class="row">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="col-md-8">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}">Usuarios</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <h2>Ficha del usuario:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $user->name }}
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <strong>Correo electrónico:</strong>
                {{ $user->email }}
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <strong>Rol:</strong>
                {{ $user->rol }}
            </div>
        </div>
    </div>
</div>
@endsection 