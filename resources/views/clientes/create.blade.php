@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Registrar nuevo cliente</h2><br  />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
    </div><br />
    @endif
    <form method="post" action="{{url('clientes')}}">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4 required">
                <label for="nombre">Nombre:</label>
                <input required type="text" class="form-control" name="nombre">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4 required">
                <label for="dni">DNI:</label>
                <input required type="text" class="form-control" name="dni">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4 required">
                <label for="celular">Celular/Principal:</label>
                <input required type="text" class="form-control" name="celular">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="alternativo">Alternativo:</label>
                <input type="text" class="form-control" name="alternativo">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="email">Correo electrónico:</label>
                <input type="text" class="form-control" name="email">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="domicilio">Domicilio:</label>
                <input type="text" class="form-control" name="domicilio">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Registrar cliente</button>
            </div>
        </div>
    </form>
</div>
@endsection
