@extends('layouts.app')

@section('content')
<div class="container justify-content-center">
    <div class="row">
        <div class="col">
            <div class="btn-group">
                <a class="btn btn-secondary" href="{{ route('clientes.index') }}">Lista de clientes</a>
                <a class="btn btn-warning" href="{{ action('ClienteController@edit', $cliente['id']) }}">Editar este cliente</a>
            </div>
        </div>
    </div>
    <div class="row">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
    </div>

    <div class="row">
        <div class="col">
            <h2>Ficha del cliente:</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $cliente->nombre }}
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="form-group">
                <strong>DNI:</strong>
                {{ $cliente->dni }}
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="form-group">
                <strong>Celular/Principal:</strong>
                {{ $cliente->celular }}
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="form-group">
                <strong>Alternativo:</strong>
                {{ $cliente->alternativo }}
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="form-group">
                <strong>Correo electrónico:</strong>
                {{ $cliente->email }}
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="form-group">
                <strong>Domicilio:</strong>
                {{ $cliente->domicilio }}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <a class="btn btn-primary {{(auth()->user()->rol=="admin"||auth()->user()->rol=="mostrador") ? '' : 'disabled' }}"  data-toggle="collapse" href="#registrar-orden" role="button" aria-expanded="false" aria-controls="registrar-orden">
                Registrar orden nueva
            </a>
            <div class="collapse" id="registrar-orden">
                <div class="card card-body">
                    <form method="post" action="{{ route('ordens.store') }}">
                        {{csrf_field()}}
                        <input type="hidden" name="cliente_id" value="{{ $cliente->id }}" />

                        <div class="row bg-cliente p-2">
                            <div class="form-group required col-lg-6">
                                <label for="producto">Producto:</label>
                                <input type="text" class="form-control" name="producto" required>
                            </div>
                            <div class="form-group required col-lg-6">
                                <label for="marca">Marca:</label>
                                <input type="text" class="form-control" name="marca" required>
                            </div>
                        </div>
                        <div class="row bg-cliente p-2">
                            <div class="form-group required col-lg-6">
                                <label for="modelo">Modelo:</label>
                                <input type="text" class="form-control" name="modelo" required>
                            </div>
                            <div class="form-group required col-lg-6">
                                <label for="serie">Serie:</label>
                                <input type="text" class="form-control" name="serie" required>
                            </div>
                        </div>
                        <div class="row bg-cliente p-2">
                            <div class="form-group required col">
                                <label for="problema">Problema:</label>
                                <textarea rows="4" class="form-control" name="problema" required></textarea>
                            </div>
                        </div>
                        <div class="row bg-cliente p-2">
                            <div class="form-group col">
                                <label for="observaciones">Accesorios y observaciones:</label>
                                <textarea rows="2" class="form-control" name="observaciones"></textarea>
                                
                                <div class="text-right mt-4"><span class="font-italic text-right">Visible para el cliente.</span></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="ubicacion">Ubicación:</label>
                                <textarea rows="2" class="form-control" name="ubicacion"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <button type="submit" class="btn btn-success">Registrar orden</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <hr />

            @include('clientes.ordensDisplay', ['ordens' => $cliente->ordens, 'cliente_id' => $cliente->id])
        </div>
    </div>
</div>
@endsection