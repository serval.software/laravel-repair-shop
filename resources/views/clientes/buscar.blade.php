@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Buscar cliente</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="get" action="{{action('ClienteController@filter')}}">
        <div class="row">
            <div class="form-group col-lg-6">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="form-group col">
                <label for="dni">DNI:</label>
                <input type="text" class="form-control" name="dni">
            </div>
            
        </div>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="celular">Celular:</label>
                <input type="text" class="form-control" name="celular">
            </div>
            <div class="form-group col-lg-6">
                <label for="alternativo">Alternativo:</label>
                <input type="text" class="form-control" name="alternativo">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="domicilio">Domicilio:</label>
                <input type="text" class="form-control" name="domicilio">
            </div>
            <div class="form-group col-lg-6">
                <label for="email">Correo electrónico:</label>
                <input type="text" class="form-control" name="email">
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group col">
                    <button type="submit" class="btn btn-success">Buscar cliente</button>
                    <button class="btn btn-outline-secondary restablecer-formulario">Restablecer</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection