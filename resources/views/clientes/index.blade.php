@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="btn-group">
                <a class="btn btn-success" href="{{ action('ClienteController@create') }}">Registrar nuevo cliente</a>
                <a class="btn btn-primary" href="{{ action('ClienteController@buscar') }}">Búsqueda avanzada</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col my-2">
            @if ($filtrado)
            <a type="button" class="btn btn-outline-primary" href="{{ route('clientes.index') }}">Eliminar filtros</a>
            @endif
        </div>
    </div>
</div>
    @if (\Session::has('success'))
        <div class="alert alert-success d-block">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>DNI</th>
                    <th>Celular</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($clientes as $cliente)
                <tr>
                    <td><a href="{{ route('clientes.show',$cliente['id']) }}">{{$cliente['nombre']}}</a></td>
                    <td>{{$cliente['dni']}}</td>
                    <td>{{$cliente['celular']}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Acciones">
                            <a href="{{action('ClienteController@show', $cliente['id'])}}" class="btn btn-info btn-sm">Ver</a>
                            <a href="{{action('ClienteController@edit', $cliente['id'])}}" class="btn btn-warning btn-sm">Editar</a>
                            <form onsubmit="return confirm('¿Realmente desea borrar al cliente? No borrará las órdenes asociadas.');" action="{{action('ClienteController@destroy', $cliente['id'])}}" method="post" class="d-inline-block confirm-needed">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
