<div class="row">
    <div class="col">
        @foreach($ordens as $orden)
        <div class="card my-4 orden">
            <a href="{{ action('OrdenController@show', $orden['id']) }}" class="link-unstyled">
                @include('ordens.estados.card-header', ['id'=>$orden['id'], 'estado' => $orden['estado']])
            </a>

            <div class="card-body">
                <h5 class="card-title">Producto: <strong>{{ $orden->producto }}</strong></h5>

                <h6 class="card-subtitle mb-2 text-muted">
                    <span>{{ $orden->marca }}</span> /
                    <span>{{ $orden->modelo }}</span> /
                    <span>{{ $orden->serie }}</span> 
                </h6>

                <p class="card-text"><strong>Problema:</strong> {!! nl2br($orden->problema) !!}</p>

                <p class="card-text"><strong>Accesorios y observaciones:</strong> {!! nl2br($orden->observaciones) !!}</p>
            </div>
            
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Ubicación:</strong> {!! nl2br($orden->ubicacion) !!}</li>
                <li class="list-group-item"><strong>Seguimiento:</strong> {!! nl2br($orden->seguimiento) !!}</li>
                <li class="list-group-item">
                    <strong>Revisión:</strong> {!! nl2br($orden->revision) !!} <br>
                    <em>Fecha: {{ $orden->fecha_revision }}</em> <br>
                    @if (!empty($orden->tecnico_user->name))
                    <strong>Técnico:</strong> {{ $orden->tecnico_user->name }} <br>
                    @endif
                    
                    @if (!empty($orden->en_garantia))
                        @if ($orden->en_garantia)
                        <strong class="text-danger">Entró por garantía</strong>
                        @else
                        <strong class="text-warning">No entró por garantía</strong>
                        @endif
                    @endif
                </li>
                <li class="list-group-item">
                    <strong>Presupuesto:</strong> {!! nl2br($orden->presupuesto) !!} <br>
                    <em>Fecha: {{ $orden->fecha_presupuesto }}</em> <br>
                    @if (!empty($orden->admin_user->name))
                    <strong>Presupuestado por:</strong> {{ $orden->admin_user->name }} <br>
                    @endif
                    
                    @if ($orden->permitir_consultas)
                    <strong>Consulta web permitida</strong>
                    @else
                    <strong class="text-warning">Consulta web no permitida</strong>
                    @endif
                </li>
                <li class="list-group-item">
                    <em>Registrado el: {{ $orden->created_at->format('d/m/Y') }}</em> <br>
                    <em>Retirado el: {{ $orden->retirado_at ? $orden->retirado_at->format('d/m/Y') : '-' }}</em>
                </li>
            </ul>

            <div class="card-footer">
                <span class="float-left">Registrado por: {{ $orden->mostrador_user->name }}</span>
                

                <a class="btn btn-sm btn-warning float-right" href="{{ action('OrdenController@edit', $orden['id']) }}">Editar</a>
                <a class="btn btn-sm btn-info float-right mr-1" href="{{ action('OrdenController@imprimir', $orden['id']) }}">Imprimir</a>
                <form onsubmit="return confirm('¿Realmente desea borrar la orden?');" action="{{action('OrdenController@destroy', $orden['id'])}}" method="post" class="d-inline-block confirm-needed float-right mr-1">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
                </form>
            </div>
        </div>
        @endforeach 
    </div>
</div>

