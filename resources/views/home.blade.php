@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">{{__('Home')}}</div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        {{__('Welcome, :Username.', ['Username'=>Auth::user()->name])}}
    </div>
</div>
@endsection
