<div class="sidebar-sticky">
    <ul class="nav flex-column">
        <li class="nav-item"><a class="nav-link {{ Request::path() ==  'clientes' ? 'active' : '' }}" href="{{ route('clientes.index') }}">Clientes</a></li>
        <li class="nav-item"><a class="nav-link {{ Request::path() ==  'ordens' ? 'active' : '' }}" href="{{ route('ordens.index') }}">Órdenes</a></li>
        <li class="nav-item"><a class="nav-link {{ Request::path() ==  'precios' ? 'active' : '' }}" href="{{ route('precios.index') }}">Lista de precios</a></li>
        <li class="nav-item"><a class="nav-link {{ Request::path() ==  'externos' ? 'active' : '' }}" href="{{ route('externos.index') }}">Externos</a></li>
        <li class="nav-item"><a class="nav-link {{ Request::path() ==  'users' ? 'active' : '' }}" href="{{ route('users.index') }}">Usuarios</a></li>
    </ul>
    <!--<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Ejemplos</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
        </a>
    </h6>
    <ul class="nav flex-column mb-2">
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Opción del menú
            </a>
        </li>
    </ul>-->
</div>