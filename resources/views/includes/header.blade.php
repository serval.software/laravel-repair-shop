<a class="navbar-brand d-none d-sm-inline-block col-sm-3 col-md-2 mr-0" href="{{ url('/') }}">
    {{ config('app.name', 'Laravel') }}
</a>
<button class="navbar-toggler d-block d-md-none" type="button" data-toggle="collapse" data-target="#sidebar" aria-controls="sidebar" aria-expanded="false" aria-label="Mostrar u ocultar barra lateral">
    <span class="navbar-toggler-icon"></span>
</button>



<form method="get" class="w-100" action="{{action('ClienteController@search')}}">
    <div class="input-group w-100">
        <input class="form-control" id="buscar-cliente" name="buscar-cliente" type="text" placeholder="Buscar cliente" aria-label="Buscar cliente">

        <span class="input-group-append">
            <button type="submit" class="btn btn-secondary">
                Buscar
            </button>
        </span>
    </div>
</form>

<ul class="navbar-nav px-3">
    @guest
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a>
    </li>
    @if (Route::has('register'))
    <li class="nav-item text-nowrap">
        <a class="nav-link" href="{{ route('register') }}">Registrarse</a>
    </li>
    @endif
    @else
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="authNavbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="authNavbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                Cerrar sesión
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </li>
    @endguest
</ul>