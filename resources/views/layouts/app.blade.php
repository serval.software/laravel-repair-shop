<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body>
    <div id="app">
        <nav id="top-nav" class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap navbar-expand p-0">
            @include('includes.header')
        </nav>

        <div class="container-fluid">
            <div class="row">
                <nav id="sidebar" class="col-6 col-md-3 col-lg-2 bg-light sidebar collapse d-md-block">
                    @include('includes.sidebar')
                </nav>

                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                    <div class="d-flex justify-content-between flex-wrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        @yield('content')
                    </div>
                    <footer class="row">
                        @include('includes.footer')
                    </footer>
                </main>


            </div>


        </div>
    </div>
</body>

</html>