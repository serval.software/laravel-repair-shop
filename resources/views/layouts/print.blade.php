<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
</head>

<body>
    <div id="app" class="container-fluid">
        <div class="row">
            <main role="main" class="col">
                @yield('content')
            </main>
        </div>
    </div>
</body>

</html>