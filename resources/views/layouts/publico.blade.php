<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('includes.head')
    <style type="text/css">
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            margin-bottom: 120px;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
        }
    </style>
</head>

<body class="d-flex flex-column h-100">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="https://gitlab/serval.software/laravel-repair-shop">Laravel Repair Shop</a>
    </nav>
    </header>
    <main role="main" class="flex-shrink-0">
        <div class="container p-4" style="margin-top: 60px;">
            @yield('content')
        </div>
    </main>
    <footer class="footer mt-auto py-3 bg-dark">
        <div class="container text-center">
            <span class="text-muted">
                Desarrollo web: <a href="https://servalweb.com">servalweb.com</a> - <a href="https://gitlab.com/serval.software/laravel-repair-shop">Código fuente</a>
            </span>
        </div>
    </footer>
</body>

</html>