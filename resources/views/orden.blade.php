@extends('layouts.publico')

@section('content')
<div class="card">
    <h4 class="card-title text-center card-header">Estado de la orden: <div class="ml-1 text-monospace bigger-text bg-info badge">{{ $orden->codigo }}</div></h4>
    <div class="card-body">
        <ul class="list-group list-group-flush mb-4">
            <li class="list-group-item bigger-text bg-cliente"><strong>Estado:</strong> {{ $orden->estadoNombre() }}</li>
            <li class="list-group-item"><strong>Producto:</strong> {{ $orden->producto }}</li>
            <li class="list-group-item"><strong>Marca:</strong> {{ $orden->marca }}</li>
            <li class="list-group-item"><strong>Modelo:</strong> {{ $orden->modelo }}</li>
            <li class="list-group-item"><strong>Serie:</strong> {{ $orden->serie }}</li>
            <li class="list-group-item"><strong>Problema:</strong>
                <p>{!! nl2br($orden->problema) !!}</p>
            </li>
            <li class="list-group-item"><strong>Accesorios y observaciones:</strong>
                <p>{!! nl2br($orden->observaciones) !!}</p>
            </li>
            <li class="list-group-item">
                <em>Registrado el: {{ $orden->created_at->format('d/m/Y') }}</em>
            </li>
            <li class="list-group-item bg-cliente">
                <em>Prespuestado el: {{ $orden->fecha_presupuesto ? $orden->fecha_presupuesto->format('d/m/Y') : '-' }}</em>
            </li>
            <li class="list-group-item bg-cliente"><strong>Presupuesto:</strong>
                <p>{!! nl2br($orden->presupuesto) !!}</p>
            </li>
            <li class="list-group-item">
                <em>Retirado el: {{ $orden->retirado_at ? $orden->retirado_at->format('d/m/Y') : '-' }}</em>
            </li>
        </ul>

        <h5 class="card-title text-center">Ficha del cliente:</h5>
        <ul class="list-group list-group-flush mb-4">
            <li class="list-group-item">
                <strong>Nombre:</strong>
                {{ asterisks($orden->cliente->nombre) }}
            </li>
            <li class="list-group-item">
                <strong>DNI:</strong>
                {{ asterisks($orden->cliente->dni, 2) }}
            </li>
            <li class="list-group-item">
                <strong>Celular/Principal:</strong>
                {{ asterisks($orden->cliente->celular, 2) }}
            </li>
            <li class="list-group-item">
                <strong>Alternativo:</strong>
                {{ asterisks($orden->cliente->alternativo, 2) }}
            </li>
            <li class="list-group-item">
                <strong>Correo electrónico:</strong>
                {{ asterisks($orden->cliente->email, 3) }}
            </li>
            <li class="list-group-item">
                <strong>Domicilio:</strong>
                {{ asterisks($orden->cliente->domicilio, 2) }}
            </li>
        </ul>

        <form method="get" class="w-100 pt-4" action="{{action('ConsultaController@orden')}}">
            <div class="input-group w-100">
                <input class="form-control" id="codigo" name="codigo" type="text" placeholder="Consultar por otra orden" aria-label="Consultar por otra orden">

                <span class="input-group-append">
                    <button type="submit" class="btn btn-success">
                        Consultar
                    </button>
                </span>
            </div>
        </form>
    </div>
</div>
@endsection