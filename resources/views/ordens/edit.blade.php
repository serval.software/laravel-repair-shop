@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Editar orden {{ $orden->codigo }}</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{action('OrdenController@update', $orden->id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">

        <div class="row">

            <div class="form-group col-lg-6">
                <label for="mostrador_user_id">Registrada por:</label>
                <select class="form-control" name="mostrador_user_id" {{ $permiso[ 'mostrador' ] ? "":"disabled" }}>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}" {{($orden->mostrador_user_id==$user->id?"selected":"")}}>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-lg-6 bg-cliente p-2">
                <label for="estado">Estado:</label>
                <select class="form-control" name="estado" {{ $permiso[ 'general' ] ? "":"disabled" }}>
                    @foreach ($estados as $index => $estado)
                    <option value="{{$index}}" {{($orden->estado==$index?"selected":"")}}>{{$estado}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row bg-cliente p-2">
            <div class="form-group col-lg-6">
                <label for="created_at">Registrado el:</label>
                <input type="date" class="form-control" name="created_at" value="{{$orden->created_at->format('Y-m-d')}}" disabled>
            </div>

            <div class="form-group col-lg-6">
                <label for="retirado_at">Retirado el:</label>
                <input type="date" class="form-control" name="retirado_at" {!!($orden->retirado_at)?'value="'.$orden->retirado_at->format('Y-m-d').'"':''!!} {{ $permiso['mostrador' ] ? "":"disabled" }}>

                <div class="text-right mt-4"><span class="font-italic text-right">Visible para el cliente.</span></div>
            </div>
        </div>

        <hr>

        <div class="row bg-cliente p-2">
            <div class="form-group col-lg-6 required">
                <label for="producto">Producto:</label>
                <input required type="text" class="form-control" name="producto" value="{{$orden->producto}}" {{ $permiso['mostrador' ] ? "":"disabled" }}>
            </div>

            <div class="form-group col-lg-6 required">
                <label for="marca">Marca:</label>
                <input required type="text" class="form-control" name="marca" value="{{$orden->marca}}" {{ $permiso['mostrador' ] ? "":"disabled" }}>
            </div>
        </div>

        <div class="row bg-cliente p-2">
            <div class="form-group col-lg-6 required">
                <label for="modelo">Modelo:</label>
                <input required type="text" class="form-control" name="modelo" value="{{$orden->modelo}}" {{ $permiso['mostrador' ] ? "":"disabled" }}>
            </div>

            <div class="form-group col-lg-6 required">
                <label for="serie">Serie:</label>
                <input required type="text" class="form-control" name="serie" value="{{$orden->serie}}" {{ $permiso[ 'mostrador' ] ? "":"disabled" }}>
            </div>
        </div>

        <div class="row bg-cliente p-2">
            <div class="form-group col required">
                <label for="problema">Problema:</label>
                <textarea required rows="2" class="form-control" name="problema" {{ $permiso[ 'mostrador' ] ? "":"disabled" }}>{{$orden->problema}}</textarea>
            </div>
        </div>

        <div class="row bg-cliente p-2">
            <div class="form-group col">
                <label for="observaciones">Accesorios y observaciones:</label>
                <textarea rows="2" class="form-control" name="observaciones" {{ $permiso[ 'mostrador' ] ? "":"disabled" }}>{{$orden->observaciones}}</textarea>

                <div class="text-right mt-4"><span class="font-italic text-right">Visible para el cliente.</span></div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col">
                <label for="ubicacion">Ubicación:</label>
                <textarea rows="2" class="form-control" name="ubicacion" {{ $permiso[ 'general' ] ? "":"disabled" }}>{{$orden->ubicacion}}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="seguimiento">Seguimiento:</label>
                <textarea rows="4" class="form-control" name="seguimiento" {{ $permiso[ 'general' ] ? "":"disabled" }}>{{$orden->seguimiento}}</textarea>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="tecnico_user_id">Revisado por:</label>
                <select class="form-control" name="tecnico_user_id" {{ $permiso[ 'tecnico' ] ? "":"disabled" }}>
                    <option value="" {{($orden->tecnico_user_id==""?"selected":"")}}>Nadie</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}" {{($orden->tecnico_user_id==$user->id?"selected":"")}}>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="fecha_revision">Revisado el:</label>
                <input name="fecha_revision" class="form-control" type="date" {!!($orden->fecha_revision)?'value="'.$orden->fecha_revision->format('Y-m-d').'"':''!!}  {{ $permiso[ 'tecnico' ] ? "":"disabled" }}>
            </div>
        </div>
        <div class="row">
            <div class="form-group col">
                <label for="revision">Revisión:</label>
                <textarea rows="4" class="form-control" name="revision"  {{ $permiso[ 'tecnico' ] ? "":"disabled" }}>{{$orden->revision}}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-check">
                    <input type="hidden" name="en_garantia" value="0" {{ $permiso[ 'tecnico' ] ? "":"disabled" }}>
                    <input type="checkbox" {{ $orden->en_garantia ? 'checked':'' }} {{ $permiso[ 'tecnico' ] ? "":"disabled" }} class="form-check-input" id="en_garantia" name="en_garantia" value="1">
                    <label class="form-check-label" for="en_garantia">Está en garantía</label>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="admin_user_id">Presupuestado por:</label>
                <select class="form-control" name="admin_user_id" {{ $permiso[ 'admin' ] ? "":"disabled" }}>
                    <option value="" {{($orden->admin_user_id==""?"selected":"")}}>Nadie</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}" {{($orden->admin_user_id==$user->id?"selected":"")}}>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6 bg-cliente p-2">
                <label for="fecha_presupuesto">Presupuestado el:</label>
                <input name="fecha_presupuesto" class="form-control" type="date" {!!($orden->fecha_presupuesto)?'value="'.$orden->fecha_presupuesto->format('Y-m-d').'"':''!!} {{ $permiso[ 'admin' ] ? "":"disabled" }}>
            </div>
        </div>

        <div class="row">
            <div class="form-group col bg-cliente p-2">
                <label for="presupuesto">Presupuesto:</label>
                <textarea rows="4" class="form-control" name="presupuesto" {{ $permiso[ 'admin' ] ? "":"disabled" }}>{{$orden->presupuesto}}</textarea>

                <div class="text-right mt-4"><span class="font-italic text-right">Visible para el cliente.</span></div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-check">
                    <input type="hidden" name="permitir_consultas" value="0" {{ $permiso[ 'admin' ] ? "":"disabled" }}>
                    <input type="checkbox" {{ $orden->permitir_consultas ? 'checked':'' }} {{ $permiso[ 'admin' ] ? "":"disabled" }} class="form-check-input" id="permitir_consultas" name="permitir_consultas" value="1">
                    <label class="form-check-label" for="permitir_consultas">Permitir consultas por la web</label>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col">
                <div class="form-group col">
                    <button type="submit" class="btn btn-success">Modificar orden</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection