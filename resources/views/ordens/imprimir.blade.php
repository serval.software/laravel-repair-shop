@extends('layouts.print')

@section('content')
<div id="hoja-impresion" class="m-4">
    <div class="row mb-4">
        <div class="col-3">
            <img src="/images/logo.png" alt="Laravel Repair Shop" width="150">
        </div>
        <div class="col-9 text-right">
            <h2 class=" text-monospace">Código de orden: <strong>{{ $orden->codigo }}</strong></h2>
            <span>ingrese a /consulta con este código para ver el estado de su orden</span>
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            <hr>
            <p>https://gitlab.com/serval.software/laravel-repair-shop</p>
            <p>DOCUMENTO NO VÁLIDO COMO FACTURA</p>
            <hr>
            <p class="text-right">Fecha: {{ $orden->created_at->format('d/m/Y') }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3>FICHA DE CLIENTE:</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 ">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $orden->cliente->nombre }}
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <strong>DNI:</strong>
                {{ $orden->cliente->dni }}
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <strong>Celular/Principal:</strong>
                {{ $orden->cliente->celular }}
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <strong>Alternativo:</strong>
                {{ $orden->cliente->alternativo }}
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <strong>Correo electrónico:</strong>
                {{ $orden->cliente->email }}
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <strong>Domicilio:</strong>
                {{ $orden->cliente->domicilio }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3>FICHA DE ENTRADA DE PRODUCTO PARA REPARACIÓN:</h3>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card my-4 orden">
                <div class="card-body">
                    <h5 class="card-title">Producto: <strong>{{ $orden->producto }}</strong></h5>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Marca:</strong> {{ $orden->marca }}</li>
                        <li class="list-group-item"><strong>Modelo:</strong> {{ $orden->modelo }}</li>
                        <li class="list-group-item"><strong>Serie:</strong> {{ $orden->serie }}</li>
                        <li class="list-group-item"><strong>Problema:</strong>
                            <p>{!! nl2br($orden->problema) !!}</p>
                        </li>
                        <li class="list-group-item"><strong>Accesorios y observaciones:</strong>
                            <p>{!! nl2br($orden->observaciones) !!}</p>
                        </li>
                        <li class="list-group-item">
                            <em>Registrado el: {{ $orden->created_at->format('d/m/Y') }}</em>
                        </li>
                        <li class="list-group-item">
                            <strong>POR CONSULTAS SOBRE EL ESTADO DE SU REPARACIÓN O PRESUPUESTO: 
                                ingrese a /consulta con el código de orden que figura en el margen superior derecho.</strong>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row text-center mt-4 pt-4">
        <div class="col-6">
            Firma: ____________________________
        </div>
        <div class="col-6">
            Aclaración: ____________________________
        </div>
    </div>
</div>

@endsection