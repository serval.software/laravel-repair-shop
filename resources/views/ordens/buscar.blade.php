@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Buscar orden</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="get" action="{{action('OrdenController@filter')}}">

        <div class="row">
            <div class="form-group col-lg-2">
                <label for="id">Número de orden:</label>
                <input class="form-control" name="id" type="number">
            </div>
            <div class="form-group col-lg-2">
                <label for="codigo">Código de orden:</label>
                <input class="form-control" name="codigo" type="text">
            </div>

            <div class="form-group col-lg-4">
                <label for="mostrador_user_id">Registrada por:</label>
                <select class="form-control" name="mostrador_user_id">
                    <option value="">Cualquiera</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-lg-4">
                <label for="estado">Estado:</label>
                <select class="form-control" name="estado">
                    <option value="">Cualquiera</option>
                    @foreach ($estados as $index => $estado)
                    <option value="{{$index}}">{{$estado}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="created_at-inicio">Registrado el / Inicio:</label>
                <input class="form-control" type="date" name="created_at-inicio">
                <label for="created_at-fin">Fin:</label>
                <input class="form-control" type="date" name="created_at-fin">
            </div>

            <div class="form-group col-lg-6">
                <label for="retirado_at-inicio">Retirado el / Inicio:</label>
                <input class="form-control" type="date" name="retirado_at-inicio">
                <label for="retirado_at-fin">Fin:</label>
                <input class="form-control" type="date" name="retirado_at-fin">
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="producto">Producto:</label>
                <input type="text" class="form-control" name="producto">
            </div>

            <div class="form-group col-lg-6">
                <label for="marca">Marca:</label>
                <input type="text" class="form-control" name="marca">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="modelo">Modelo:</label>
                <input type="text" class="form-control" name="modelo">
            </div>

            <div class="form-group col-lg-6">
                <label for="serie">Serie:</label>
                <input type="text" class="form-control" name="serie">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="problema">Problema:</label>
                <input type="text" class="form-control" name="problema">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="observaciones">Accesorios y observaciones:</label>
                <input type="text" class="form-control" name="observaciones">
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col">
                <label for="ubicacion">Ubicación:</label>
                <input type="text" class="form-control" name="ubicacion">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="seguimiento">Seguimiento:</label>
                <input type="text" class="form-control" name="seguimiento">
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="tecnico_user_id">Revisado por:</label>
                <select class="form-control" name="tecnico_user_id">
                    <option value="">Cualquiera</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="fecha_revision-inicio">Revisado el / Inicio:</label>
                <input class="form-control" type="date" name="fecha_revision-inicio">
                <label for="fecha_revision-fin">Fin:</label>
                <input class="form-control" type="date" name="fecha_revision-fin">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="revision">Revisión:</label>
                <input type="text" class="form-control" name="revision">
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="en_garantia" name="en_garantia" value="1">
                    <label class="form-check-label" for="en_garantia">Está en garantía</label>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="admin_user_id">Presupuestado por:</label>
                <select class="form-control" name="admin_user_id">
                    <option value="">Cualquiera</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="fecha_presupuesto-inicio">Presupuestado el / Inicio:</label>
                <input class="form-control" type="date" name="fecha_presupuesto-inicio">
                <label for="fecha_presupuesto-fin">Fin:</label>
                <input class="form-control" type="date" name="fecha_presupuesto-fin">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <label for="presupuesto">Presupuesto:</label>
                <input type="text" class="form-control" name="presupuesto">
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="form-group col">
                    <button type="submit" class="btn btn-success">Buscar orden</button>

                    <button class="btn btn-outline-secondary restablecer-formulario">Restablecer</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection