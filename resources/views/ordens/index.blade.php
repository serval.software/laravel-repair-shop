@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="btn-group">
                <a class="btn btn-primary" href="{{ action('OrdenController@search') }}">Búsqueda avanzada</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col my-2">
            <a href="{{action('OrdenController@filter', 'grupo=revision')}}" class="badge badge-danger">A revisar</a>
            <a href="{{action('OrdenController@filter', 'grupo=presupuestos')}}" class="badge badge-warning">(Re)presupuestar</a>
            <a href="{{action('OrdenController@filter', 'grupo=comunicacion')}}" class="badge badge-primary">Comunicación</a>
            <a href="{{action('OrdenController@filter', 'grupo=reparacion')}}" class="badge badge-secondary">Reparación</a>
            <a href="{{action('OrdenController@filter', 'grupo=retirar')}}" class="badge badge-success">A retirar</a>
            <a href="{{action('OrdenController@filter', 'grupo=retirados')}}" class="badge badge-light">Retirados</a>
            <a href="{{action('OrdenController@filter', 'grupo=sinretirar')}}" class="badge badge-dark">Sin retirar</a>
        </div>
    </div>
    <div class="row">
        <div class="col my-2">
            @if ($filtrado)
            <a type="button" class="btn btn-outline-primary" href="{{ route('ordens.index') }}">Eliminar filtros</a>
            @endif
        </div>
    </div>
</div>

@if (\Session::has('success'))
<div class="alert alert-success">
    <p>{{ \Session::get('success') }}</p>
</div><br />
@endif
<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
            <form method="get" id="filtrarForm" action="{{action('OrdenController@filter')}}">
                <tr>
                    <th class="position-relative">
                        <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarId" role="button" aria-expanded="false" aria-controls="filtrarId">
                            Número
                            @if (!empty(app('request')->input('id')))
                            <i class="fas fa-filter"></i>
                            @endif
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <div class="collapse form-group filtro-columna" id="filtrarId">
                            <input class="form-control" name="id" type="number" value="{{ app('request')->input('id') }}">
                            <a class="input-clear" href="#filtrarId"><i class="fas fa-times-circle"></i></a>
                        </div>
                    </th>
                    <th class="position-relative">
                        <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarCodigo" role="button" aria-expanded="false" aria-controls="filtrarCodigo">
                            Código
                            @if (!empty(app('request')->input('id')))
                            <i class="fas fa-filter"></i>
                            @endif
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <div class="collapse form-group filtro-columna" id="filtrarCodigo">
                            <input class="form-control" name="codigo" type="text" value="{{ app('request')->input('codigo') }}">
                            <a class="input-clear" href="#filtrarCodigo"><i class="fas fa-times-circle"></i></a>
                        </div>
                    </th>
                    <th class="position-relative">
                        <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarEstado" role="button" aria-expanded="false" aria-controls="filtrarEstado">
                            Estado
                            @if (!empty(app('request')->input('estado')))
                            <i class="fas fa-filter"></i>
                            @endif
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <div class="collapse form-group filtro-columna" id="filtrarEstado">
                            <select class="form-control auto-submit" name="estado">
                                <option value="">Todos</option>
                                @foreach ($estados as $index => $estado)
                                <option value="{{$index}}" {{(app('request')->input('estado')==$index?"selected":"")}}>{{$estado}}</option>
                                @endforeach
                            </select>
                        </div>
                    </th>
                    <th class="position-relative">
                        <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarProducto" role="button" aria-expanded="false" aria-controls="filtrarProducto">
                            Producto
                            @if (!empty(app('request')->input('producto')))
                            <i class="fas fa-filter"></i>
                            @endif
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <div class="collapse form-group filtro-columna" id="filtrarProducto">
                            <input class="form-control" name="producto" type="text" value="{{ app('request')->input('producto') }}">
                            <a class="input-clear" href="#filtrarProducto"><i class="fas fa-times-circle"></i></a>
                        </div>
                    </th>
                    <!--<th>Técnico</th>-->
                    <th class="position-relative">
                        <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarFechaRegistro" role="button" aria-expanded="false" aria-controls="filtrarFechaRegistro">
                            Registrado
                            @if (!empty(app('request')->input('created_at-inicio')))
                            <i class="fas fa-filter"></i>
                            @endif
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <div class="collapse form-group filtro-columna card p-2" id="filtrarFechaRegistro">
                            <label for="created_at-inicio">Inicio:</label>
                            <input class="form-control" type="date" name="created_at-inicio" value="{{ app('request')->input('created_at-inicio') }}">
                            <label for="created_at-fin">Fin:</label>
                            <input class="form-control" type="date" name="created_at-fin" value="{{ app('request')->input('created_at-fin') }}">
                        </div>
                    </th>
                    <th class="position-relative">
                        <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarFechaRetiro" role="button" aria-expanded="false" aria-controls="filtrarFechaRetiro">
                            Retirado
                            @if (!empty(app('request')->input('retirado_at-inicio')))
                            <i class="fas fa-filter"></i>
                            @endif
                            <i class="fas fa-caret-down"></i>
                        </a>
                        <div class="collapse form-group filtro-columna card p-2" id="filtrarFechaRetiro">
                            <label for="retirado_at-inicio">Inicio:</label>
                            <input class="form-control" type="date" name="retirado_at-inicio" value="{{ app('request')->input('retirado_at-inicio') }}">
                            <label for="retirado_at-fin">Fin:</label>
                            <input class="form-control" type="date" name="retirado_at-fin" value="{{ app('request')->input('retirado_at-fin') }}">
                        </div>
                    </th>
                    <th>Acciones
                        <input type="submit" class="d-none" value="Filtrar">
                    </th>
                </tr>
            </form>
        </thead>
        <tbody>
            @foreach($ordens as $orden)
            <tr>
                <td>
                    <span class="badge badge-dark text-monospace">{{ $orden->id }}</span>
                </td>
                <td>
                    <span class="badge badge-light text-monospace">{{ $orden->codigo }}</span> 
                </td>
                <td>
                    @include('ordens.estados.badge-pill', ['estado' => $orden['estado']])
                </td>
                <td>{{$orden['producto']}}</td>
                <!--<td>
                    @if (empty($orden['updater']))
                    <span>Sin asignar</span>
                    @else
                    <a href="{{  route('users.show',$orden['updater']['id']) }}">{{$orden['updater']['name']}}</a>
                    @endif
                </td>-->
                <td>{{ $orden->created_at->format('d/m/Y') }}</td>
                <td>{{ $orden->retirado_at ? $orden->retirado_at->format('d/m/Y') : '-' }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Acciones">
                        <a href="{{action('OrdenController@show', $orden['id'])}}" class="btn btn-info btn-sm">Ver</a>
                        <a href="{{action('OrdenController@edit', $orden['id'])}}" class="btn btn-warning btn-sm">Editar</a>
                        <form onsubmit="return confirm('¿Realmente desea borrar la orden?');" action="{{action('OrdenController@destroy', $orden['id'])}}" method="post" class="d-inline-block confirm-needed">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">
                            <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{ $ordens->links() }}
</div>
@endsection