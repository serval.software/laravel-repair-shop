@if ($orden->estado == App\Orden::INGRESADO_A_REVISAR)
<div class="card-header bg-danger">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span> 
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@elseif ($orden->estado == App\Orden::REVISADO_A_PRESUPUESTAR || $orden->estado == App\Orden::REPRESUPUESTAR)
<div class="card-header bg-warning">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span>
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@elseif ($orden->estado == App\Orden::PRESUPUESTADO_A_PREGUNTAR || $orden->estado == App\Orden::PARA_RETIRAR_AVISAR || $orden->estado == App\Orden::EN_ESPERA_DE_RESPUESTA)
<div class="card-header bg-primary">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span>
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@elseif ($orden->estado == App\Orden::A_REPARAR || $orden->estado == App\Orden::EN_REPARACION || $orden->estado == App\Orden::EN_PREPARACION)
<div class="card-header bg-secondary text-light">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span>
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@elseif ($orden->estado == App\Orden::PARA_RETIRAR_AVISADO || $orden->estado == App\Orden::PLAZO_VENCIDO_AVISADO)
<div class="card-header bg-success">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span>
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@elseif ($orden->estado == App\Orden::RETIRADO_REPARADO || $orden->estado == App\Orden::RETIRADO_NO_REPARADO)
<div class="card-header bg-light">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span>
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@elseif ($orden->estado == App\Orden::NO_RETIRADO_RECICLADO || $orden->estado == App\Orden::A_RECICLAR)
<div class="card-header bg-dark text-light">
    {{ $orden->estadoNombre() }}
    <span class="float-right badge badge-light text-monospace bigger-text">{{ $orden->codigo }}</span>
    <span class="float-right badge badge-dark text-monospace bigger-text mr-1">{{ $orden->id }}</span>
</div>
@endif