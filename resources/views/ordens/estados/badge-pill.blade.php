@if ($orden->estado == App\Orden::INGRESADO_A_REVISAR)
<span class="badge badge-pill badge-danger">{{$orden->estadoNombre()}}</span>
@elseif ($orden->estado == App\Orden::REVISADO_A_PRESUPUESTAR || $orden->estado == App\Orden::REPRESUPUESTAR)
<span class="badge badge-pill badge-warning">{{$orden->estadoNombre()}}</span>
@elseif ($orden->estado == App\Orden::PRESUPUESTADO_A_PREGUNTAR || $orden->estado == App\Orden::PARA_RETIRAR_AVISAR || $orden->estado == App\Orden::EN_ESPERA_DE_RESPUESTA)
<span class="badge badge-pill badge-primary">{{$orden->estadoNombre()}}</span>
@elseif ($orden->estado == App\Orden::A_REPARAR || $orden->estado == App\Orden::EN_REPARACION || $orden->estado == App\Orden::EN_PREPARACION)
<span class="badge badge-pill badge-secondary">{{$orden->estadoNombre()}}</span>
@elseif ($orden->estado == App\Orden::PARA_RETIRAR_AVISADO || $orden->estado == App\Orden::PLAZO_VENCIDO_AVISADO)
<span class="badge badge-pill badge-success">{{$orden->estadoNombre()}}</span>
@elseif ($orden->estado == App\Orden::RETIRADO_REPARADO || $orden->estado == App\Orden::RETIRADO_NO_REPARADO)
<span class="badge badge-pill badge-light">{{$orden->estadoNombre()}}</span>
@elseif ($orden->estado == App\Orden::NO_RETIRADO_RECICLADO || $orden->estado == App\Orden::A_RECICLAR)
<span class="badge badge-pill badge-dark">{{$orden->estadoNombre()}}</span>
@endif