@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Registrar nueva orden</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
    </div><br />
    @endif
    <form method="post" action="{{url('ordens')}}">
        {{csrf_field()}}
        <div class="visible-cliente">
            <div class="row">
                <div class="form-group col-lg-6 required mx-auto">
                    <label for="producto">Producto:</label>
                    <input type="text" class="form-control" required name="producto">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 required mx-auto">
                    <label for="marca">Marca:</label>
                    <input type="text" class="form-control" required name="marca">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 required mx-auto">
                    <label for="modelo">Modelo:</label>
                    <input type="text" class="form-control" required name="modelo">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 required mx-auto">
                    <label for="serie">Serie:</label>
                    <input type="text" class="form-control" required name="serie">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 required mx-auto">
                    <label for="problema">Problema:</label>
                    <textarea rows="4" class="form-control" required name="problema"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 mx-auto">
                    <label for="observaciones">Accesorios y observaciones:</label>
                    <textarea rows="2" class="form-control" name="observaciones"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-6 mx-auto">
                <label for="ubicacion">Ubicación:</label>
                <textarea rows="2" class="form-control" name="ubicacion"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-6 mx-auto">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Registrar orden</button>
            </div>
        </div>
    </form>
</div>
@endsection