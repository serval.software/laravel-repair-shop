@extends('layouts.publico')

@section('content')
<div class="card text-center">
    <h4 class="card-title text-center card-header">Consulta de estado de orden de reparación</h4>
    <div class="card-body">
        

        @if ($busquedaFail)
        <div class="alert alert-danger">
            No se pudo encontrar la orden, por favor intente nuevamente.
        </div>
        @endif

        <p class="card-text">Ingrese el código de orden que figura en la parte superior derecha de su comprobante</p>

        <form method="get" class="w-100" action="{{action('ConsultaController@orden')}}">
            <div class="input-group w-100">
                <input class="form-control" id="codigo" name="codigo" type="text" placeholder="Ingrese el código aquí" aria-label="Ingrese el código aquí">

                <span class="input-group-append">
                    <button type="submit" class="btn btn-success">
                        Consultar
                    </button>
                </span>
            </div>
        </form>

    </div>
</div>
@endsection