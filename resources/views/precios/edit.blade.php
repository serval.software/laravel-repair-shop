@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Editar precio</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{action('PrecioController@update', $precio->id)}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">

        <div class="row">
            <div class="form-group col-lg-6">
                <label for="categoria">Categoría:</label>
                <select class="form-control" name="categoria">
                    @foreach ($categorias as $index => $categoria)
                    <option value="{{$index}}" {{($precio->categoria==$index?"selected":"")}}>{{$categoria}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="externo_id">Proveedor:</label>
                <select class="form-control" name="externo_id">
                    <option value="">Ninguno</option>
                    @foreach ($externos as $externo)
                    <option value="{{$externo->id}}" {{($precio->externo_id==$externo->id?"selected":"")}}>{{$externo->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4">
                <label for="marca">Marca:</label>
                <input type="text" class="form-control" name="marca" value="{{$precio->marca}}">
            </div>
            <div class="form-group col-lg-4">
                <label for="modelo">Modelo:</label>
                <input type="text" class="form-control" name="modelo" value="{{$precio->modelo}}">
            </div>
            <div class="form-group col-lg-4 required">
                <label for="parte">Parte:</label>
                <input required type="text" class="form-control" name="parte" value="{{$precio->parte}}">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4 required">
                <label for="precio">Precio del proveedor:</label>
                <input required type="text" class="form-control" name="precio" value="{{$precio->precio}}">
            </div>
            <div class="form-group col-lg-4">
                <label for="precio_cliente">Precio para el cliente:</label>
                <input type="text" class="form-control" name="precio_cliente" value="{{$precio->precio_cliente}}">
            </div>
            <div class="form-group col-lg-4">
                <label for="fecha_precio">Fecha del precio:</label>
                <input name="fecha_precio" class="form-control" type="date" value="{{$precio->fecha_precio?$precio->fecha_precio->format('Y-m-d'):''}}">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <button type="submit" class="btn btn-success">Modificar precio</button>
            </div>
        </div>
    </form>
</div>
@endsection