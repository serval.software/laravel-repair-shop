@if ($precio->categoria == App\Precio::PLACAS)
<span class="badge badge-pill badge-success">{{$precio->categoriaNombre()}}</span>
@else
<span class="badge badge-pill badge-warning">{{$precio->categoriaNombre()}}</span>
@endif