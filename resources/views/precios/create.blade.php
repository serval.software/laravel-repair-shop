@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Registrar nuevo precio</h2><br />
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
    </div><br />
    @endif
    <form method="post" action="{{url('precios')}}">
        {{csrf_field()}}
        <div class="row">
            <div class="form-group col-lg-6">
                <label for="categoria">Categoría:</label>
                <select class="form-control" name="categoria">
                    @foreach ($categorias as $index => $categoria)
                    <option value="{{$index}}">{{$categoria}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="externo_id">Proveedor:</label>
                <select class="form-control" name="externo_id">
                    <option value="">Ninguno</option>
                    @foreach ($externos as $externo)
                    <option value="{{$externo->id}}">{{$externo->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4">
                <label for="marca">Marca:</label>
                <input type="text" class="form-control" name="marca">
            </div>
            <div class="form-group col-lg-4">
                <label for="modelo">Modelo:</label>
                <input type="text" class="form-control" name="modelo">
            </div>
            <div class="form-group col-lg-4 required">
                <label for="parte">Parte:</label>
                <input required type="text" class="form-control" name="parte">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-4 required">
                <label for="precio">Precio del proveedor:</label>
                <input required type="text" class="form-control" name="precio">
            </div>
            <div class="form-group col-lg-4">
                <label for="precio_cliente">Precio para el cliente:</label>
                <input type="text" class="form-control" name="precio_cliente">
            </div>
            <div class="form-group col-lg-4">
                <label for="fecha_precio">Fecha del precio:</label>
                <input name="fecha_precio" class="form-control" type="date">
            </div>
        </div>

        <div class="row">
            <div class="form-group col">
                <button type="submit" class="btn btn-success">Registrar precio</button>
            </div>
        </div>
    </form>
</div>
@endsection