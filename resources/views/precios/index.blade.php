@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="btn-group">
                <a class="btn btn-primary" href="{{ action('PrecioController@create') }}">Registrar precio</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col my-2">
            @if ($filtrado)
            <a type="button" class="btn btn-outline-primary" href="{{ route('precios.index') }}">Eliminar filtros</a>
            @endif
        </div>
    </div>
</div>
    @if (\Session::has('success'))
        <div class="alert alert-success d-block">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
    @endif
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <form method="get" id="filtrarForm" action="{{action('PrecioController@filter')}}">
                    <tr>
                        <th class="position-relative">
                            <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarCategoria" role="button" aria-expanded="false" aria-controls="filtrarCategoria">
                                Categoría
                                @if (!empty(app('request')->input('categoria')))
                                <i class="fas fa-filter"></i>
                                @endif
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="collapse form-group filtro-columna" id="filtrarCategoria">
                                <select class="form-control auto-submit" name="categoria">
                                    <option value="">Todos</option>
                                    @foreach ($categorias as $index => $categoria)
                                    <option value="{{$index}}" {{(app('request')->input('categoria')==$index?"selected":"")}}>{{$categoria}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </th>
                        <th class="position-relative">
                            <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarMarca" role="button" aria-expanded="false" aria-controls="filtrarMarca">
                                Marca
                                @if (!empty(app('request')->input('marca')))
                                <i class="fas fa-filter"></i>
                                @endif
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="collapse form-group filtro-columna" id="filtrarMarca">
                                <input class="form-control" name="marca" type="text" value="{{ app('request')->input('marca') }}">
                                <a class="input-clear" href="#filtrarMarca"><i class="fas fa-times-circle"></i></a>
                            </div>
                        </th>
                        <th class="position-relative">
                            <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarModelo" role="button" aria-expanded="false" aria-controls="filtrarModelo">
                                Modelo
                                @if (!empty(app('request')->input('modelo')))
                                <i class="fas fa-filter"></i>
                                @endif
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="collapse form-group filtro-columna" id="filtrarModelo">
                                <input class="form-control" name="modelo" type="text" value="{{ app('request')->input('modelo') }}">
                                <a class="input-clear" href="#filtrarModelo"><i class="fas fa-times-circle"></i></a>
                            </div>
                        </th>
                        <th class="position-relative">
                            <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarParte" role="button" aria-expanded="false" aria-controls="filtrarParte">
                                Parte
                                @if (!empty(app('request')->input('parte')))
                                <i class="fas fa-filter"></i>
                                @endif
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="collapse form-group filtro-columna" id="filtrarParte">
                                <input class="form-control" name="parte" type="text" value="{{ app('request')->input('parte') }}">
                                <a class="input-clear" href="#filtrarParte"><i class="fas fa-times-circle"></i></a>
                            </div>
                        </th>
                        <th>Precio Prov.</th>
                        <th>Precio Cliente</th>
                        <th class="position-relative">
                            <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarFecha" role="button" aria-expanded="false" aria-controls="filtrarFecha">
                                Fecha
                                @if (!empty(app('request')->input('fecha-inicio')))
                                <i class="fas fa-filter"></i>
                                @endif
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="collapse form-group filtro-columna card p-2" id="filtrarFecha">
                                <label for="fecha-inicio">Inicio:</label>
                                <input class="form-control" type="date" name="fecha-inicio" value="{{ app('request')->input('fecha-inicio') }}">
                                <label for="fecha-fin">Fin:</label>
                                <input class="form-control" type="date" name="fecha-fin" value="{{ app('request')->input('fecha-fin') }}">
                            </div>
                        </th>
                        <th class="position-relative">
                            <a class="d-flex w-100 justify-content-between align-items-center" data-toggle="collapse" href="#filtrarProveedor" role="button" aria-expanded="false" aria-controls="filtrarProveedor">
                                Proveedor
                                @if (!empty(app('request')->input('externo_id')))
                                <i class="fas fa-filter"></i>
                                @endif
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <div class="collapse form-group filtro-columna" id="filtrarProveedor">
                                <select class="form-control auto-submit" name="externo_id">
                                    <option value="">Todos</option>
                                    @foreach ($proveedores as $proveedor)
                                    <option value="{{$proveedor->id}}" {{(app('request')->input('externo_id')==$proveedor->id?"selected":"")}}>{{$proveedor->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </th>
                        <th>Acciones
                            <input type="submit" class="d-none" value="Filtrar">
                        </th>
                    </tr>
                </form>
            </thead>
            <tbody>
            @foreach($precios as $precio)
                <tr>
                    <td>@include('precios.categorias.badge-pill', ['categoria' => $precio['categoria']])</td>
                    <td>{{$precio['marca']}}</td>
                    <td>{{$precio['modelo']}}</td>
                    <td>{{$precio['parte']}}</td>
                    <td>{{$precio['precio']}}</td>
                    <td>{{$precio['precio_cliente']}}</td>
                    <td>{{$precio['fecha_precio']?$precio->fecha_precio->format('d/m/y'):'-'}}</td>
                    <td>{{$precio->externo_id?$precio->proveedor->nombre:'-'}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Acciones">
                            <a href="{{action('PrecioController@edit', $precio['id'])}}" class="btn btn-warning btn-sm">Editar</a>
                            <form onsubmit="return confirm('¿Realmente desea borrar al precio?');" action="{{action('PrecioController@destroy', $precio['id'])}}" method="post" class="d-inline-block confirm-needed">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-sm" type="submit">Borrar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
