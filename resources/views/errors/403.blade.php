@extends('layouts.app')

@section('content')
    <div class="alert alert-danger">
        <h4>Error 403: {{ $exception->getMessage() }}</h4>
    </div>
@endsection
