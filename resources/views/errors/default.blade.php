@extends('layouts.app')

@section('content')
    <div class="alert alert-danger">
        <h4>{!! $exception !!}</h4>
    </div>
@endsection
