
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*
const app = new Vue({
    el: '#app'
});
*/
function acomodar(){
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var navbarHeight = document.getElementById('top-nav').clientHeight;
    var sidebar = document.getElementById('sidebar')
    sidebar.style.top = navbarHeight+"px";
    sidebar.style.height = (h-navbarHeight)+"px";
}
document.addEventListener('DOMContentLoaded', function(){ 
    setTimeout(acomodar, 100);
}, false);
window.addEventListener('resize', function(event){
    acomodar();
});

$('#sidebar').on('shown.bs.collapse', function (e) {
    acomodar();
})

$(".auto-submit").change(function() {
    $("#filtrarForm").submit();
});

$(".input-clear").on("click", function(e){
    e.preventDefault();
    var target = $(this).attr("href");
    $(target).children("input").val("").focus();
})

$('.filtro-columna').on('shown.bs.collapse', function(e) {
    $(this).children("input").focus();
})

$('input[name="created_at-inicio"]').on('change', function(e) {
    $inputFin = $('input[name="created_at-fin"]');
    if (( $inputFin.val() == '' || $inputFin.val() < $(this).val() ) && $(this).val() != '') {
        $inputFin.val($(this).val());
    }
})

$('input[name="created_at-fin"]').on('change', function(e) {
    $inputInicio = $('input[name="created_at-inicio"]');
    if (( $inputInicio.val() == '' || $inputInicio.val() > $(this).val() ) && $(this).val() != '') {
        $inputInicio.val($(this).val());
    }
})

$('input[name="retirado_at-inicio"]').on('change', function(e) {
    $inputFin = $('input[name="retirado_at-fin"]');
    if (( $inputFin.val() == '' || $inputFin.val() < $(this).val() ) && $(this).val() != '') {
        $inputFin.val($(this).val());
    }
})

$('input[name="retirado_at-fin"]').on('change', function(e) {
    $inputInicio = $('input[name="retirado_at-inicio"]');
    if (( $inputInicio.val() == '' || $inputInicio.val() > $(this).val() ) && $(this).val() != '') {
        $inputInicio.val($(this).val());
    }
})

$('input[name="fecha-inicio"]').on('change', function(e) {
    $inputFin = $('input[name="fecha-fin"]');
    if (( $inputFin.val() == '' || $inputFin.val() < $(this).val() ) && $(this).val() != '') {
        $inputFin.val($(this).val());
    }
})

$('input[name="fecha-fin"]').on('change', function(e) {
    $inputInicio = $('input[name="fecha-inicio"]');
    if (( $inputInicio.val() == '' || $inputInicio.val() > $(this).val() ) && $(this).val() != '') {
        $inputInicio.val($(this).val());
    }
})

$('input[name="fecha_revision-inicio"]').on('change', function(e) {
    $inputFin = $('input[name="fecha_revision-fin"]');
    if (( $inputFin.val() == '' || $inputFin.val() < $(this).val() ) && $(this).val() != '') {
        $inputFin.val($(this).val());
    }
})

$('input[name="fecha_revision-fin"]').on('change', function(e) {
    $inputInicio = $('input[name="fecha_revision-inicio"]');
    if (( $inputInicio.val() == '' || $inputInicio.val() > $(this).val() ) && $(this).val() != '') {
        $inputInicio.val($(this).val());
    }
})

$('input[name="fecha_presupuesto-inicio"]').on('change', function(e) {
    $inputFin = $('input[name="fecha_presupuesto-fin"]');
    if (( $inputFin.val() == '' || $inputFin.val() < $(this).val() ) && $(this).val() != '') {
        $inputFin.val($(this).val());
    }
})

$('input[name="fecha_presupuesto-fin"]').on('change', function(e) {
    $inputInicio = $('input[name="fecha_presupuesto-inicio"]');
    if (( $inputInicio.val() == '' || $inputInicio.val() > $(this).val() ) && $(this).val() != '') {
        $inputInicio.val($(this).val());
    }
})

$('button.restablecer-formulario').on('click', function(e) {
    e.preventDefault();
    $(this).parents("form").trigger("reset");
})