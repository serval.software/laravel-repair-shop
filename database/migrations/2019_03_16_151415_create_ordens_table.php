<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->unique()->nullable();
            
            /* admin_orden_editar */
            /* orden_crear/editar (mostrador) */
            $table->integer('cliente_id')->unsigned();
            $table->integer('mostrador_user_id')->unsigned();
            $table->string('producto');
            $table->string('marca');
            $table->string('modelo');
            $table->string('serie');
            $table->text('problema');
            $table->text('observaciones')->nullable();

            /* orden_revision (tecnicos) */
            $table->text('ubicacion')->nullable();
            $table->tinyInteger('estado')->unsigned();
            $table->text('seguimiento')->nullable();
            $table->date('retirado_at')->nullable();
            /* fin_orden_crear */
            
            $table->integer('tecnico_user_id')->unsigned()->nullable();
            $table->text('revision')->nullable();
            $table->date('fecha_revision')->nullable();
            $table->boolean('en_garantia')->default(false);
            /* fin_orden_revision */

            $table->integer('admin_user_id')->unsigned()->nullable();
            $table->text('presupuesto')->nullable();
            $table->date('fecha_presupuesto')->nullable();
            $table->boolean('permitir_consultas')->default(true);
            /* fin_admin_orden_editar */

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordens');
    }
}
