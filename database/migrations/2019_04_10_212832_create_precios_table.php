<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precios', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('categoria')->unsigned();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('parte')->nullable();
            $table->string('precio')->nullable();
            $table->string('precio_cliente')->nullable();
            $table->date('fecha_precio')->nullable();
            $table->integer('externo_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precios');
    }
}
