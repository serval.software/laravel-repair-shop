## Laravel Repair Shop

This is a simple system for a electronics repair shop.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).

Laravel Repair Shop is open-source software licensed under the [GPL v3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).